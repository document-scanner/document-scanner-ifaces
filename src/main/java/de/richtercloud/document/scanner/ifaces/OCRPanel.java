/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.ifaces;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * A panel which display the result of OCR of the area selected in the associated {@link OCRSelectComponent}.
 *
 * The relation to the associated components {@link OCRSelectComponent} and {@link EntityPanel} is explained in
 * {@link MainPanel}.
 *
 * @author richter
 */
public abstract class OCRPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    public abstract JTextArea getoCRResultTextArea();

    public abstract JCheckBox getTrimWhitespaceCheckBox();

    public abstract MainPanel getMainPanel();
}
