/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.ifaces;

import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceExecutor;
import javax.swing.JPanel;

/**
 * A component holding the {@link de.richtercloud.reflection.form.builder.ReflectionFormPanel} most likely in a
 * containing component.
 *
 * The relation to the associated components {@link OCRSelectComponent} and {@link OCRPanel} is explained in
 * {@link MainPanel}.
 *
 * Manages auto-OCR-value-detection and the result values since they're
 * associated with entity data.
 *
 * @author richter
 */
public abstract class EntityPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    /**
     * Runs the process of value detection (including a call to
     * {@link #valueDetectionGUI() }).
     *
     * @param oCRSelectPanelPanelFetcher the fetcher to use
     * @param forceRenewal allows to force detection of values no matter which
     *     values already have been recognized
     */
    public abstract void valueDetection(OCRSelectPanelPanelFetcher oCRSelectPanelPanelFetcher,
            boolean forceRenewal);

    /**
     * Clears all component model representing auto-OCR-value-detection results
     * and fills them with already retrieved data. This is useful if more
     * {@link de.richtercloud.reflection.form.builder.ReflectionFormPanel}s are added which weren't considered before
     * in which case it's unnecessary to run the complete detection again.
     */
    public abstract void valueDetectionGUI();

    public abstract ValueDetectionServiceExecutor<?> getValueDetectionServiceExecutor();

    public abstract MainPanel getMainPanel();
}
