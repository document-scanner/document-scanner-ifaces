/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.valuedetectionservice;

import de.richtercloud.test.tools.TestRandomUtils;
import java.util.Locale;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author richter
 */
public class ValueDetectionServiceTest {

    @Test
    public void testRetrieveLanguageIdentifier() {
        String expResult = ValueDetectionService.LANGUAGE_ENGLISH;
        String result = ValueDetectionService.retrieveLanguageIdentifier(Locale.ENGLISH);
        Assert.assertEquals(expResult, result);
        expResult = ValueDetectionService.LANGUAGE_CHINESE;
        result = ValueDetectionService.retrieveLanguageIdentifier(Locale.CHINESE);
        Assert.assertEquals(expResult, result);
        expResult = ValueDetectionService.LANGUAGE_FRENCH;
        result = ValueDetectionService.retrieveLanguageIdentifier(Locale.FRENCH);
        Assert.assertEquals(expResult, result);
        expResult = ValueDetectionService.LANGUAGE_GERMAN;
        result = ValueDetectionService.retrieveLanguageIdentifier(Locale.GERMAN);
        Assert.assertEquals(expResult, result);
        expResult = ValueDetectionService.LANGUAGE_SPANISH;
        result = ValueDetectionService.retrieveLanguageIdentifier(Locale.forLanguageTag("es"));
        Assert.assertEquals(expResult, result);
        Locale[] availableLocales = Locale.getAvailableLocales();
        Locale locale = availableLocales[TestRandomUtils.getInsecureTestRandom().nextInt(availableLocales.length)];
        expResult = locale.getLanguage();
        result = ValueDetectionService.retrieveLanguageIdentifier(locale);
        Assert.assertEquals(expResult, result);
    }
}
